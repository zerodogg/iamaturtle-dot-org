#!/usr/bin/perl
# iamaturtle.org
#
# Copying and redistribution allowed under the terms of the Creative Commons
# Attribution-ShareAlike 4.0 License.

use strict;
use utf8;
use POSIX qw(strftime);
use open qw/:std :utf8/;
use utf8;
use 5.020;

# Real languages.
# Try to keep the list sorted-ish. If it's not 100% accurately sorted
# it doesn't matter, it is sorted before displayed anyway.
my %Languages = (
    'Afrikaans'             => "Ek is 'n skilpad",
    'Amharic'               => 'Eli negn',
    'Ancient Egyptian'      => 'Inik shety-ew',
    'Arabic'                => 'Ana sulhufat',
    'Armenian'              => 'Ես կրիա եմ (Es kria em)',
    'Albanian'              => 'Un&euml; jam nj&euml; breshk&euml;',
    'Basque'                => 'Dordoka bat naiz',
    'Bengali'               => 'মি একটি কচ্ছপ (Ami ekti kocchop)',
    'Bulgarian'             => 'Аз съм костенурка (Az sum kostenurka)',
    'Catalan'               => 'Soc una tortuga',
    'Chinese'               => 'wo3 shi4 zhi3 wu1 gui1',
    'Classical Attic Greek' => 'Ego eimi khelonos',
    'Czech'                 => 'Jsem želva',
    'Croatian'              => 'Ja sam kornjača',
    'Danish'                => 'Jeg er en skildpadde',
    'Dutch'                 => 'Ik ben een schildpad',
    'English'               => 'I am a turtle',
    'Estonian'              => 'Ma olen kilpkonn',
    'Faroese'               => 'Eg eri ein havskjalb&oslash;ka',
    'Finnish'               => 'Olen kilpikonna',
    'Filipino (Tagalog)'    => 'Ako ay isang pagong',
    'French'                => 'Je suis une tortue',
    'German'                => 'Ich bin eine Schildkr&ouml;te',
    'Georgian'              => 'მე კუს ვარ  (me kus var)',
    'Greek'                 => 'Eimai Xelona',
    'Gujarti'               => 'Hu kachbo chuu',
    'Hebrew'                => 'Ani tsav',
    'Hindi'                 => 'Main kachawa hoon',
    'Hungarian'             => 'Tekn&eth;s vagyok',
    'Icelandic'             => '&Eacute;g er skjaldbaka',
    'Indonesian'            => 'Saya kura-kura',
    'Irish'                 => 'Is turtar m&eacute;',
    #	'Irish Gaelic' => 'Is mise turtar',
    'Italian'            => 'Sono una tartaruga',
    'Japanese'           => '私は亀です (Watashi wa kame desu)',
    'Javanese'           => 'Aku kuro',
    'Korean (universal)' =>
      '저는 거북 입니다 (Jeoneun Geobuk Imnida)<br />나는 거북 입니다 (Naneun Geobuk Imnida)',
    'Korean (South Korean standard)' =>
      '저는 거북 입니다 (Jeoneun Geobuk Imnida)<br />나는 거북 입니다 (Naneun Geobuk Imnida)',
    'Jeju' =>
      '저는 거북 있수다 (Jeoneun Geobuk Itsuda)<br />나는 거북 있수다 (Naneun Geobuk Itsuda)',
    'Kurdish, Sorani'        => 'Min K&icirc;selem',
    'Latin'                  => 'Testudo sum',
    'Lithuanian'             => 'Aš esu vėžlys',
    'Maltese'                => 'Jiena fekruna',
    'Mina (TOGO)'            => 'Eklo mou gni',
    'Norwegian bokm&aring;l' => 'Jeg er en skilpadde',
    'Norwegian nynorsk'      => 'Eg er ei skilpadde',
    'Nepali'                 => 'म कछुवा हुँ (Ma Kachhuwaa Hu)',
    'Persian'                => 'M&auml;n Yek Lakp&aring;sht H&auml;st&auml;m',
    'Polish'                 => 'Jestem &#380;&#243;&#322;wiem',
    'Portuguese'             => 'Eu sou uma tartaruga',
    'Russian'                => 'Ya cherepakha',
    'Romanian'               => 'Sunt o țestoasă',
    'Serbian'                => 'Ja sam kornjaca',
    'Slovak'                 => 'Som korytnačka',
    'Slovenian'              => 'Jaz sem zelva',
    'Spanish'                => 'Soy una tortuga',
    'Sundanese'              => 'Abdi kuya',
    'Swedish'                => 'Jag &auml;r en sk&ouml;ldpadda',
    'Tamil'                  => 'Naan oru aamai',
    'Urdu'                   => 'میں نے ایک کچھی ہوں (Main ek kachua hoon)',
    'Turkish'                => 'Ben kaplumbagayim',
    'Thai'                   => 'Chan pen tao',
    'Vietnamese'             => 'Con rua',
    'Welsh'                  => "Dwi'n grwban m&ocirc;r",
);
# Not real languages. The same guidelines applies.
my %NotRealLanguages = (
    'Australian' => 'Ahm a bloody turtle, mate',
    'Binary'     =>
'01001001001000000110000101<br/>10110100100000011000010010<br/>00000111010001110101011100<br/>10011101000110110001100101',
    'Cockney'              => 'Oi am ai ture\'al',
    'Darth Vader'          => 'Luke, I am a turtle',
    'Morse code (English)' =>
'<code style="font-size:12pt">..     .- --     .-     - ..- .-. - .-.. .</code>',
    'Egg language'      => 'Eggi eggam egga teggurteggle',
    'English leetspeek' => '| 4|\/| 4 7|_||2713',
    'Esperanto'         => 'Mi estas testudo',
    'Hexadecimal'       =>
      '0x490x200x610x6D0x200x610x200<br />x740x750x720x740x6C0x65',
    'Ido'       => 'Me es tortugon',
    'Octal'     => '111 40 141 155 40 141 40<br/>164 165 162 164 154 145',
    'Pig Latin' =>
      'Iay amay a urtletay<br/>I am a urtletay<br/>Iay amay aay urtletay',
    'Pigeon (aka. Gibberish)' => 'Elegi alegam alega telagurtlegle',
    'Politically correctness' =>
'I am a reptilian creature who has a hard covering<br />on its back that travels in a very slow motion',
    'Scientific speak' => 'I exist as a chelonian',
    'Wingmakers'       => 'RAaaHghGrrRAwrarahgh',
    'Ulster Scots'     => 'Ama unnerwatter tortus hey',
);

# Purpose: Print the head
# Usage: print html_head();
sub html_head
{
    return
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title>I Am A Turtle dot org</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="copyright" content="Eskild Hustvedt"/>
		<style type="text/css" media="screen">
		th{
			font-weight:bold;
			border: 1px solid #000000;
			padding:0.2em 0.5em 0.2em 0.5em;
		}
		td{
			border: 1px solid #000000;
			padding:0.2em 0.5em 0.2em 0.5em;
		}
		.footer{font-style:italic; font-size: small;}
		#about{margin:0 50% 0 0;}
		h1{font-size: xx-large;}
		h2{font-size: large;}
		h3{font-size: medium;}
		img{border:none;}
		</style>
	</head>';
}

# Purpose: Begin the main body
# Usage: print html_beginmain();
sub html_beginmain
{
    my $main = '<body>
	<table style="border:0;" border="0"><tr><td style="border:0;">
		<h1>I am a turtle</h1>
		<h2>Je suis une tortue</h2>';
    $main .=
        "\n<p>Real languages: "
      . scalar(keys(%Languages))
      . " - <i>Not so real languages: "
      . scalar(keys(%NotRealLanguages))
      . "</i></p>\n";
    $main .= '
	<table summary="I am a turtle">
			<tbody>
				<tr>
					<th scope="col">Language:</th>
					<th scope="col">I am a turtle:</th>
				</tr>';
}

# Purpose: Output a language hash
# Usage: html_hashformat(\%Hash);
sub html_hashformat
{
    my $href = shift;
    foreach my $l (sort(keys(%{$href})))
    {
        print '				<tr>' . "\n";
        print '					<td>' . $l . "</td>\n";
        print '					<td>' . $href->{$l} . "</td>\n";
        print '				</tr>' . "\n";
    }
}

# Purpose: End the main section
# Usage: print html_endmain();
sub html_endmain
{
    my $data =
'</tbody></table></td><td style="border:0;" valign="top"></td></tr></table>';
    return $data;
}

# Purpose: Get about info
# Usage: print html_about();
sub html_about
{
    my $About = '<div id="about">
			<h3>About</h3>

			<p>Welcome to I Am a Turtle dot org. A rather pointless website.</p>

			<p>Want to contribute an additional translation, see a typo or just want to contact me?
			Mail me: <a href="mailto:iamaturtle [at] zerodogg [dot] org">iamaturtle [at] zerodogg [dot] org</a><br/>
			I\'m getting a lot of e-mails concerning this site, and I read all of them and try to respond to most.
			As I get so much it can take some time before I reply, for this I apologize.<br/><br/>
			I wish to thank all the people that have contributed translations to iamaturtle.org,
			without your contributions this site would not exist.<br /><br /><i>Page last updated on ';
    $About .= scalar localtime();
    my $currYear = strftime('%Y', localtime);
    my $years    = join(', ', 2004 .. $currYear);
    $About .= '.</i><br /><br/></p>
		</div>
		<p><a href="http://www.panda.org/what_we_do/endangered_species/marine_turtles/"><img src="https://www.iamaturtle.org/wwf.png" alt="Save the turtles!"/></a></p>
		<p class="footer">Copyright &copy; <a href="https://www.eskild.dev/">Eskild Hustvedt<a/> <a href="https://www.zerodogg.org/">(Zero_Dogg)</a> '
      . $years . ' - Some Rights Reserved<br/>
		Copying allowed under the terms of the <a href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0</a> License.<br />
        iamaturtle.org uses <a href="https://www.goatcounter.com/">GoatCounter</a> for privacy-respecting statistics.
		</p>';
    return $About;
}

# Purpose: End the entire doc
# Usage: print html_end();
sub html_end
{
    return
'<script data-goatcounter="https://iamaturtle.goatcounter.com/count" async src="//gc.zgo.at/count.v1.js" crossorigin="anonymous" integrity="sha384-RD/1OXO6tEoPGqxhwMKSsVlE5Y1g/pv/Pf2ZOcsIONjNf1O+HPABMM4MmHd3l5x4"></script>
    </body>
</html>';
}

print html_head() . "\n";
print html_beginmain() . "\n";
html_hashformat(\%Languages);
html_hashformat(\%NotRealLanguages);
print html_endmain() . "\n";
print html_about() . "\n";
print html_end();

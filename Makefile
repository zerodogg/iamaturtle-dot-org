SHELL=/bin/bash
MINIFY=$(shell if which gominify &>/dev/null; then echo "gominify";else echo "minify";fi)
default: build
distrib: build compress
build: clean
	perl generate.pl > public/index.html
	cp 404.html public/
	wget --random-wait --retry-connrefused -t 10 -O public/wwf.png 'https://web.archive.org/web/20180605085708im_/https://www.iamaturtle.org/wwf.png'
	wget https://raw.githubusercontent.com/ai-robots-txt/ai.robots.txt/refs/tags/v1.23/robots.txt -O public/robots.txt
clean:
	rm -rf public
	mkdir -p public
compress:
	for ftype in html; do\
		for file in $$(find public \( -name "*.$$ftype" \)); do \
			mv "$$file" "$$file.tmp";\
			cat "$$file.tmp"|$(MINIFY) --html-keep-document-tags --type "$$ftype" -o "$$file";\
			rm -f "$$file.tmp";\
		done; \
	done
	find public \( -name '*.html' -o -name '*.ics' -o -name '*.json' -o -name '*.css' -o -name '*.js' \) -print0 | xargs -0 gzip -9 -k
